#!/usr/bin/python2.7

from ip import get_ip
from dbg import debug

import smtplib

def send_mail( flag, fromaddr, toaddrs, msg, server, port, usr, pwd ):

	ip = get_ip()

	if ip == 0:
		debug('URL Error, can\'t send :S')
		return 0

	msg += 'http://' + ip + '/\n'

	if flag != 0:
		msg += 'Crah della rete :S [%s]' % flag

	try:
		server = smtplib.SMTP(server + ':' + port)
		#server.set_debuglevel(1)
		server.starttls()
		server.login(usr,pwd)
		server.sendmail(fromaddr, toaddrs, msg)
		server.quit()
	except:
		return -1

	return ip;
