# IPToolZ

**IPToolZ** is a IP address monitor. It finds router external IP
address and if it changes, sends a mail to a configurable address mail.

**IPToolZ** stores a day splitted full LOG of all output.
The **log files** have this file name format : *DD.MM.YYYY.log* 

The **config file** (.cfg) must be in */opt/iptoolz/iptoolz.cfg*.
Settings list of config file :

 * **from**    --> mail address from 
 * **to**      --> mail address to
 * **msg**     --> extended mail message
 * **server**  --> SMTP server
 * **port**    --> SMTP server port
 * **usr**     --> SMTP server user (crypted)
 * **pwd**     --> SMTP server password (crypted)

**Manual Install** :

1.	git clone ecc ecc...

2. 	mkdir /opt/iptoolz/log

3.	Edit */opt/iptoolz/iptoolz.cfg* with correct param

4.	*/opt/iptoolz/config.py mail@gmail.com password_mail*

5.	Use the output of *config.py* as **usr** and **pwd** parameter.
