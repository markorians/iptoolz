#!/usr/bin/env python2.7

from ip 	import get_ip
from dbg	import debug, debug_no_time, get_time, wait
from sendmail 	import send_mail
from crypt	import crypt
from config 	import get_config

import sys

debug('Service Started ...')
debug('Loading config file ...')

try:
	vect = get_config()
except:
	debug('Error on config file, exit...')
	debug_no_time('\n\n')
	exit(-1)

if vect==-1:
	debug('Config file not exist, exit...')
	debug_no_time('\n\n')
	exit(-1)

fromaddr = vect[0]
toaddrs  = vect[1]
msg  	 = vect[2]
server   = vect[3]
port 	 = vect[4]
usr 	 = vect[5]
pwd 	 = vect[6]

debug('Config OK')

debug_no_time ("\t\t\tFrom : %s" 	% fromaddr			)
debug_no_time ("\t\t\tTo : %s" 	% toaddrs			)
debug_no_time ("\t\t\tMSG : %s" 	% msg.replace("\n","\n\t\t\t")	)
debug_no_time ("\t\t\tServer : %s" 	% server			)
debug_no_time ("\t\t\tPort : %s" 	% port				)
debug_no_time ("\t\t\tUser : %s" 	% usr				)
debug_no_time ("\t\t\tPass : %s" 	% crypt(pwd)			)

ip = get_ip()
debug('Find IP as %s' % ip)

flag = 0

try:
	while 1:

		wait(5)

		if ip == get_ip():
			if ip != 0:
				debug('IP not changed')
			else:
				debug('Offline')
		else :
			debug('IP WAS CHANGED :( Sending mail ...')
			ip = send_mail(flag, fromaddr, toaddrs, msg, server, port, usr, pwd)
			if ip != 0:
				debug('Mail sent :P')
			else :
				if ip == -1
					debug('Mail Socket Exception')
				flag = get_time()
except:
	debug('Caught exception : %s' % sys.exc_info()[0])
	debug('Service Stopped ...')
	debug_no_time('\n\n')
