#!/usr/bin/python2.7

from crypt import crypt,decrypt
import ConfigParser, sys, getopt, os

def get_config():
	dir = "/opt/iptoolz"
	config = ConfigParser.RawConfigParser()
	isread = config.read( dir + '/iptoolz.cfg')

	if isread == []:
		return -1;

	fromaddr = config.get('Config', 'from');
	toaddr 	 = config.get('Config', 'to');
	msg 	 = config.get('Config', 'msg');
	server 	 = config.get('Config', 'server'); 
	port 	 = config.get('Config', 'port');
	usr 	 = decrypt( config.get('Config', 'usr') );
	pwd 	 = decrypt( config.get('Config', 'pwd') );

	return [fromaddr,toaddr,msg,server,port,usr,pwd];

def main():
	if len(sys.argv) <= 1:
		print "Usage: config.py <user> <password>"
	else:
		print "USR -->" + crypt(sys.argv[1])
		print "PWD -->" + crypt(sys.argv[2])

if __name__ == "__main__":
    main()
