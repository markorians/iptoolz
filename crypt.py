#!/usr/bin/python2.7

def crypt( string ):

	str = ""

	for c in string:
		str += chr(ord(c)^ord('X'))

	formatted = ":".join("{:02x}".format(ord(c)) for c in str)

	return formatted

def decrypt( string ):

	final=""

	str = string.replace(":","")
	decoded = str.decode("hex")

	for c in decoded:
		final += chr(ord(c)^ord('X'))

	return final
