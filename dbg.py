#!/usr/bin/python2.7

import time, os

def file_open()

	dir = "/opt/iptoolz"

	if not os.path.isfile( ('%s/log/%s.log') % (dir, time.strftime('%d.%m.%Y')) ):

		f = open( ('%s/log/%s.log') % (dir, time.strftime('%d.%m.%Y')), "w" )

	else:

		f = open( ('%s/log/%s.log') % (dir, time.strftime('%d.%m.%Y')), "a" )

	return f

def debug( str ):

	f = file_open()

	f.write(('[%s] %s\n') % (time.strftime('%d/%m/%Y - %H:%M:%S'),str))
	f.close()

	print ('[%s] %s') % (time.strftime('%d/%m/%Y - %H:%M:%S'),str)

	return

def debug_no_time( str ):

	f = file_open()

	f.write('%s\n' % str)
	f.close()

	print str

	return

def get_time():

	return time.strftime('%d/%m/%Y - %H:%M:%S')

def wait( n ):

	time.sleep(n)
	return
