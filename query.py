#!/usr/bin/python2.7

import subprocess, sys

def shell(cmd):
	return subprocess.check_output(cmd, shell=True)

def compact(string):
	string = string.split('\n')

	c = 1

	while c < len(string):
		# if string with index C contains "not" or "Offline"
		if string[c].find("not") != -1 or string[c].find("Offline") != -1:
			# if string with index C-1 contains "not" or "Offline"
			if string[c-1].find("not") != -1 or string[c-1].find("Offline") != -1:
				del string[c]
			else:
				c += 1
		else:
			c += 1
	return string

def query(vect):
	file = "cat " + vect[0]

	try:
		if len(vect) == 1 :
				return shell(file).split('\n')
		if len(vect) == 2 :
			if vect[1] == '1':
				return compact(shell(file))
			else:
				file += " | grep " + vect[1]
				return shell(file).split('\n')

		if len(vect) == 3 :
			if vect[1] == '1':
				file += " | grep " + vect[2]
				return compact(shell(file))
			else:
				file += " | grep " + vect[1]
				if vect[2] == '1':
					return compact(shell(file))
				else:
					return shell(file).split('\n')
	except:
		return -1;

def main():
	if len(sys.argv) < 2:
		print "Usage: query.py file.log str_grep [1]"
		print "       if 1 is present compact log ;) "
	else:
		del sys.argv[0]
		ret = query(sys.argv)

		if ret == -1:
			print "Shell Non Zero result Exception"
		else:
			for str in ret:
				print str
if __name__ == "__main__":
    main()
